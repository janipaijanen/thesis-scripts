#!/usr/bin/env bash

# get title, and url
function set_id(){
    existing=0
    added=0

    if [ -d $1 ] ; then

        idx=0

        for i in $(find "$1" -type f -iname article-url* ) ; do 
            idx=$((idx + 1))
            srcdir=$(dirname $i)
            id_file=${srcdir}/analysis-id.txt

            if [ ! -f $id_file ] ; then
                b_idvalue=$(date +"%Y%d%m_%H%M%S.%3N")-$idx
                b_idvalue=$(echo $b_idvalue | sha512sum | cut -c1-32 | rev | cut -c1-6)
                idvalue="z"
                idvalue+=${b_idvalue}
                # echo $idvalue
                #break                
                echo $idvalue > $id_file
                #echo "analysis-id.txt was written to $srddir"
                added=$((added+1))
            else
                #echo "NTL. analysis-id.txt alread in $srcdir"
                existing=$((existing+1))
                echo Existing file in $(dirname $i)
            fi
        done
    fi

    echo "ID was added to $added directories"
    echo "ID was skipped for $existing already assigned dirs"
}

set_id $1