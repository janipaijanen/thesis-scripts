#!/usr/bin/env bash

function xxget_licenses() {

    if [ -d "$1" ] ; then
        for i in $(find "$1" -type f -iname arcticle-lic-* ) ; do 
            content=$(cat $i)
            echo $i;$content
        done
    fi
}

# get title, and url
function get_id_title_and_url_as_csv(){
    
    header_printed=0

    if [ -d "$1" ] ; then

        for i in $(find "$1" -type f -iname article-url* ) ; do 
            srcdir=$(dirname $i)
            had_pdf=0
            is_licence_notok=0
            cite_RIS=0
            cite_BIB=0
            cite_TXT=0
        
            abstract_exists=0
            abstract_cyber_range=0
            abstract_training=0
            abstract_learning=0

            title_cyber_range=0
            title_training=0
            title_learning=0
        
            html_ok=0
            txt_ok=0
            id=$(cat $srcdir/analysis-id.txt)
            url=$(cat $i)

            # ------- TITLE
            for title_file in $(find "$srcdir" -type f -iname article-title.txt ) ; do 
                title=$(cat $title_file)

                title_cyber_range=$(cat $title_file | tr '[:upper:]' '[:lower:]' | grep -noc "cyber range")
                title_training=$(cat $title_file |tr '[:upper:]' '[:lower:]' | grep -noc training)
                title_learning=$(cat $title_file |tr '[:upper:]' '[:lower:]' | grep -noc learning) 
                break
            done


            # ------ ABSTRACT
            for abstract_file in $(find "$srcdir" -type f -iname article-abstract*txt ) ; do 
                abstract_exists=1

                abstract_cyber_range=$(cat $abstract_file | tr '[:upper:]' '[:lower:]' | grep -noc "cyber range")
                abstract_training=$(cat $abstract_file |tr '[:upper:]' '[:lower:]' | grep -noc training)
                abstract_learning=$(cat $abstract_file |tr '[:upper:]' '[:lower:]' | grep -noc learning) 
                
                break
            done

            #  TITLE AND ABSTRACT

            
            

            if [ "$title" == "" ] ; then
                title="No title file found"
            fi

            # ------- LICENSE

            for lic_file in $(find "$srcdir" -type f -iname arcticle-lic-* ) ; do 
                #echo $lic_file
                lic=$(cat $lic_file)
                is_licence_notok=$(echo $lic | grep -o 'NOK' | wc -l)

               # echo "NOK:$is_licence_notok"
                break
            done

            # ------- URL, i.e. PDF Only?
            had_pdf=$(cat $i |   tr '[:upper:]' '[:lower:]' | grep -noc -e "\.pdf")
            #echo "PDF: $had_pdf"
            # continue, by check if missing pdf file indicator exists
            
            if [ -f ${srcdir}/article-missing-pdf-file.txt ] ; then
                had_pdf=$(($had_pdf +1))
            else
                #echo "NO RIS"
                : # NOP
            fi



            # ------ CONTENT HTML
             for html_file in $(find "$srcdir" -type f -iname article-content-*.html ) ; do 
                if [ -f $html_file ] ; then
                    html_ok=1 
                fi
                break
            done
            
            # ------ CONTENT TXT
            for txt_file in $(find "$srcdir" -type f -iname article-content-*.txt ) ; do 
                if [ -f $txt_file ] ; then
                    txt_ok=1 
                fi
                break
            done

            # CITE RIS
            if [ -f ${srcdir}/article_citation.ris ] ; then
                cite_RIS=1 
            else
                #echo "NO RIS"
                : # NOP
            fi

            # CITE BIBTEX
            if [ -f ${srcdir}/article_citation.bib ] ; then
                cite_BIB=1 
            else
                #echo "NO BIB"
                : # NOP
            fi

            # CITE TEXT
            if [ -f ${srcdir}/article_citation.text ] ; then
                cite_TXT=1 
            else
                # echo "NO TEXT"
                : # NOP
            fi
            
            if [ $header_printed -eq 0 ] ; then
                header="id;srcdir;url;title;is_licence_notok;had_pdf;html_ok;txt_ok;cite_RIS;cite_BIB;cite_TXT;abstract_exists;abstract_cyber_range;abstract_training;abstract_learning;title_cyber_range;title_training;title_learning"
                echo $header
                header_printed=1
            fi
            #
            echo "$id;$srcdir;$url;$title;$is_licence_notok;$had_pdf;$html_ok;$txt_ok;$cite_RIS;$cite_BIB;$cite_TXT;$abstract_exists;$abstract_cyber_range;$abstract_training;$abstract_learning;$title_cyber_range;$title_training;$title_learning"

            
            
        done
    fi
}


get_id_title_and_url_as_csv $1